package com.epam.codingstories.credential.stuffing.controller;

import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

  @GetMapping("/emails")
  public ResponseEntity<List<String>> getAllEmails() {
    return ResponseEntity.ok(List.of(
        "a@demo.com",
        "b@demo.com"
    ));
  }
}
