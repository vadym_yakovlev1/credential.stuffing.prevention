package com.epam.codingstories.credential.stuffing.auth;

import java.util.List;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component
public class CustomIdentityAuthenticationProvider implements AuthenticationProvider {
  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {

    String username = authentication.getName();
    String password = authentication.getCredentials().toString();
    if (!isAuthenticated(username, password)) {
      throw new BadCredentialsException("Login failed due to invalid credentials");
    }
    return new UsernamePasswordAuthenticationToken(
        username,
        password, List.of());
  }

  @Override
  public boolean supports(Class<?> authentication) {
    return true;
  }

  private boolean isAuthenticated(final String userName, final String password) {
    return "user@demo.com".equals(userName) &&
        "P@ssw0rd".equals(password);
  }
}
