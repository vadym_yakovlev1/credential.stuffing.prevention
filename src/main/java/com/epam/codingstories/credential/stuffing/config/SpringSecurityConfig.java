package com.epam.codingstories.credential.stuffing.config;

import jakarta.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
public class SpringSecurityConfig {

  @Bean
  SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {

    return http.csrf().disable()
        .authorizeHttpRequests()
        .anyRequest()
        .authenticated()
        .and()
        .httpBasic()
        .realmName("demo")
        .authenticationEntryPoint((request, response, authException) -> {
          response.addHeader("WWW-Authenticate", "Basic realm=demo");
          response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
          PrintWriter writer = response.getWriter();
          writer.println("HTTP Status 401 - " + authException.getMessage());
        })
        .and()
        .build();

  }



}
