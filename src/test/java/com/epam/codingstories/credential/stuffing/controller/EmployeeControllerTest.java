package com.epam.codingstories.credential.stuffing.controller;

import com.epam.codingstories.credential.stuffing.Application;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Application.class)
class EmployeeControllerTest {

  @Autowired
  private TestRestTemplate template;

  @Test
  public void should_not_allow_username_password_login() throws Exception {
    ResponseEntity<String> result = template.withBasicAuth("user@demo.com", "P@ssw0rd")
        .getForEntity("/employees/emails", String.class);
    Assertions.assertEquals(HttpStatus.UNAUTHORIZED, result.getStatusCode());
  }
}